using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour 
{
    public GameObject uiWindow;
    public Text textField;

    public void ToggleUI(bool val)
    {
        uiWindow.SetActive(val);
    }

    public void ShowText(string text)
    {
        ToggleUI(true);
        textField.text = text;
    }



}
