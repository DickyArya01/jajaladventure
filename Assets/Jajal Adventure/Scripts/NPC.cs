using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public abstract class NPC : MonoBehaviour
{
    public UnityEvent OnSpeak;
    public AudioSource audioSource;

    public virtual void Interact()
    {
        OnSpeak?.Invoke();

        audioSource.Play();


    }

}
