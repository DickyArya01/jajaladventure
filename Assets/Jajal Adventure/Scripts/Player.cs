﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{

    private IMovementInput movementInput;
    private PlayerMovement playerMovement;
    private PlayerAnimation playerAnimation;
    private PlayerRenderer playerRenderer;
    private PlayerAIInteraction playerAIInteraction;

    public UIController uIController;


    private void Start()
    {
        movementInput = GetComponent<IMovementInput>();
        playerMovement = GetComponent<PlayerMovement>();
        playerRenderer = GetComponent<PlayerRenderer>();
        playerAIInteraction = GetComponent<PlayerAIInteraction>();
        playerAnimation = GetComponent<PlayerAnimation>();

        movementInput.OnInteractEvent += () => playerAIInteraction.Interact(playerRenderer.IsSpriteFlipped);

    }

    private void FixedUpdate()
    {

        playerMovement.MovePlayer(movementInput.MovementInputVector);
        playerRenderer.RenderPlayer(movementInput.MovementInputVector);
        playerAnimation.SetupAnimation(movementInput.MovementInputVector);

        if (movementInput.MovementInputVector.magnitude > 0)
        {
            uIController.ToggleUI(false);
        }
    }


    public void ReceiveDamaged()
    {
        playerRenderer.FlashRed();
    }

}
