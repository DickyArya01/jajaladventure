using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMobileInput : MonoBehaviour, IMovementInput
{
    public Vector2 MovementInputVector { get; private set; } 

    public event Action OnInteractEvent;

    public void GetMovementDirection(Vector2 movementDirection)
    {
        MovementInputVector = movementDirection;
    }

    public void ResetInput()
    {
        MovementInputVector = Vector2.zero;
    }

    public void GetInteraction()
    {
        OnInteractEvent?.Invoke();
    }
    
}
