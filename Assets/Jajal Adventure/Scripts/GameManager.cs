using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private string LevelScene; 
    private void Start()
    {
        SceneManager.LoadScene(LevelScene, LoadSceneMode.Additive);
    }

}
