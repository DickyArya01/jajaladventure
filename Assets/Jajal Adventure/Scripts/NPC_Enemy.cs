﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class NPC_Enemy : NPC 
{


    public override void Interact()
    {
        base.Interact();
        FindObjectOfType<Player>().ReceiveDamaged();
    }


}
