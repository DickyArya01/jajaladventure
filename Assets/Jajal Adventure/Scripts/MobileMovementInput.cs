using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MobileMovementInput : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    
    public PlayerMobileInput input;

    public Vector2 movementDirection;

    public void OnPointerDown(PointerEventData eventData)
    {
        input.GetMovementDirection(movementDirection);

    }

    public void OnPointerUp(PointerEventData eventData)
    {
        input.ResetInput(); 
    }
}