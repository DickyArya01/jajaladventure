using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimation : MonoBehaviour
{
    public Animator playerAnimator;

    public void SetupAnimation(Vector2 movementVector)
    {
       if (movementVector.magnitude > 0)
       {
            playerAnimator.SetBool("Walk", true);
       }
       else
       {
            playerAnimator.SetBool("Walk", false);
       }
    }
}

